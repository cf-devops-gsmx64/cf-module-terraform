variable "project_main_folder" {
  description = "Default main folder for the project"
  type    = string
  default = "mi_sitio_web"
}

variable "subfolder_assets" {
  description = "Assets folder for the project"
  type    = string
  default = "assets"
}

variable "subfolder_assets_css" {
  description = "CSS folder for the project"
  type    = string
  default = "css"
}

variable "subfolder_assets_js" {
  description = "JS folder for the project"
  type    = string
  default = "js"
}

variable "subfolder_pages" {
  description = "Pages folder for the project"
  type    = string
  default = "pages"
}

variable "subfolder_pages_index_file" {
  description = "Index file for the project"
  type    = string
  default = "index.html"
}

variable "subfolder_pages_about_file" {
  description = "About file for the project"
  type    = string
  default = "about.html"
}

variable "index_content" {
  description = "Content for the index file"
  type    = string
  default = "<html>\n<head>\n<title>Welcome to my website</title>\n</head>\n<body>\n<h1>Hello, world!</h1>\n</body>\n</html>"
}

variable "about_content" {
  description = "Content for the about file"
  type    = string
  default = "<html>\n<head>\n<title>About me</title>\n</head>\n<body>\n<h1>About me</h1>\n<p>I'm a web developer</p>\n</body>\n</html>"
}
