terraform {
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "2.5.1"
    }
  }
}

provider "local" {
  # Configuration options
}

resource "local_file" "project_main" {
  filename = "${var.project_main_folder}/"
  directory_permission = "0777"
  content  = ""
}

resource "local_file" "subfolder_assets" {
  filename = "${var.project_main_folder}/${var.subfolder_assets}/"
  directory_permission = "0777"
  content  = ""
}

resource "local_file" "subfolder_assets_css" {
  filename = "${var.project_main_folder}/${var.subfolder_assets}/${var.subfolder_assets_css}/"
  directory_permission = "0777"
  content  = ""
}

resource "local_file" "subfolder_assets_js" {
  filename = "${var.project_main_folder}/${var.subfolder_assets}/${var.subfolder_assets_js}/"
  directory_permission = "0777"
  content  = ""
}

resource "local_file" "subfolder_pages" {
  filename = "${var.project_main_folder}/${var.subfolder_pages}/"
  directory_permission = "0777"
  content  = ""
}

resource "local_file" "subfolder_pages_index_file" {
  filename = "${var.project_main_folder}/${var.subfolder_pages}/${var.subfolder_pages_index_file}"
  file_permission = "0777"
  content  = var.index_content
}

resource "local_file" "subfolder_pages_about_file" {
  filename = "${var.project_main_folder}/${var.subfolder_pages}/${var.subfolder_pages_about_file}"
  file_permission = "0777"
  content  = var.about_content
}

